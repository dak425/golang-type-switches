package main

import "fmt"

// Normal represents a Normal type move or Pokemon
type Normal struct{}

// Fighting represents a Fighting type move or Pokemon
type Fighting struct{}

// Flying represents a Flying type move or Pokemon
type Flying struct{}

// Poison represents a Poison type move or Pokemon
type Poison struct{}

// Ground represents a Ground type move or Pokemon
type Ground struct{}

// Rock represents a Rock type move or Pokemon
type Rock struct{}

// Bug represents a Bug type move or Pokemon
type Bug struct{}

// Ghost represents a Ghost type move or Pokemon
type Ghost struct{}

// Steel represents a Steel type move or Pokemon
type Steel struct{}

// Fire represents a Fire type move or Pokemon
type Fire struct{}

// Water represents a Water type move or Pokemon
type Water struct{}

// Grass represents a Grass type move or Pokemon
type Grass struct{}

func (g Grass) Modifier(types ...interface{}) float64 {
	modifier := 1.0

	for _, t := range types {
		switch t.(type) {
		case Fire, Flying, Bug, Poison, Steel, Grass, Dragon:
			modifier /= 2
		case Rock, Ground, Water:
			modifier *= 2
		}
	}

	return modifier
}

// Electric represents a Electric type move or Pokemon
type Electric struct{}

// Psychic represents a Psychic type move or Pokemon
type Psychic struct{}

// Ice represents a Ice type move or Pokemon
type Ice struct{}

// Dragon represents a Dragon type move or Pokemon
type Dragon struct{}

// Dark represents a Dark type move or Pokemon
type Dark struct{}

// Fairy represents a Fairy type move or Pokemon
type Fairy struct{}

func main() {
	myPokemon := Grass{}
	rival := Fire{}
	brock := []interface{}{Rock{}, Ground{}}
	mod := myPokemon.Modifier(rival)
	fmt.Printf("My %#v pokemon has a %.2f modifier against %#v\n", myPokemon, mod, rival)
	mod2 := myPokemon.Modifier(brock...)
	fmt.Printf("My %#v pokemon has a %.2f modifier against %#v\n", myPokemon, mod2, brock)
}
